#pragma once

#include <string>
#include <stdexcept>

using std::string;

class parser {
public:
	enum class mode {
		md2,
		compress,
		preimage
	};

	mode parse_request(char const **argv) {
		string mode = string(argv[1]);
		if (mode == string("md2")) {
			return mode::md2;
		} else if (mode == string("compress")) {
			return mode::compress;
		} else if (mode == string("preimage")) {
			return mode::preimage;
		} else {
			throw std::invalid_argument("invalid args");
		}
	}
};