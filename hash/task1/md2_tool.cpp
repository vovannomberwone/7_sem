#include "md2_tool.h"

#include <stdexcept>
#include <stdint.h>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <chrono>

using std::cout;
using std::endl;
using std::string;
using std::to_string;

using namespace std::chrono;

void md2_tool::run(const parser::mode m, char const **bytes16, const int ct) {
	this->bytes16 = bytes16;
	blocks = ct;

	switch (m) {
		case parser::mode::md2: {
			md2();
			cout << gen_str(hash) << endl;
		} break;
		case parser::mode::compress: {
			compress(str_to_int(string(bytes16[0])), str_to_int(string(bytes16[1])));
			cout << gen_str(hash) << endl;
		} break;
		case parser::mode::preimage: {
			preimage();
		} break;
		default: {
			throw std::runtime_error("internal error");
		}
	}
}

uint32_t md2_tool::str_to_int(string s) {
	uint32_t m = 0x00;
	uint32_t cur_byte;

	for (int i = 0; i < 16; i++) {
		m <<= 2;
		cur_byte = uint32_t(s[2 * i] - '0');
		m |= cur_byte;
	}
	return m;
}

uint32_t md2_tool::steps_1_2(const std::string s, uint32_t &checksum, uint32_t &L) {
	uint32_t m = 0x00;
	uint32_t cur_byte, tmp;

	for (int i = 0; i < 16; i++) {
		m <<= 2;
		cur_byte = uint32_t(s[2 * i] - '0');
		m |= cur_byte;
		tmp = S_proc(cur_byte ^ L);
		checksum ^= (tmp << (30 - 2 * i));
		L = (checksum & (0x3 << (30 - 2 * i))) >> (30 - 2 * i);
	}
	return m;
}

uint32_t md2_tool::steps_1_2_last(const std::string s, uint32_t &checksum, uint32_t &L, bool &is_devided) {
	uint32_t m = 0x00;
	uint32_t cur_byte, tmp;
	int len = s.length();

	for (int i = 0; i < (len + 1) / 2; i++) {
		m <<= 2;
		cur_byte = uint32_t(s[2 * i] - '0');
		m |= cur_byte;
		tmp = S_proc(cur_byte ^ L);
		checksum ^= (tmp << (30 - 2 * i));
		L = (checksum & (0x3 << (30 - 2 * i))) >> (30 - 2 * i);
	}
	for (int i = (len + 1) / 2; i < 16; i++) {
		m <<= 2;
		cur_byte = uint32_t((16 - len) % 4);
		m |= cur_byte;
		tmp = S_proc(cur_byte ^ L);
		checksum ^= (tmp << (30 - 2 * i));
		L = (checksum & (0x3 << (30 - 2 * i))) >> (30 - 2 * i);
	}

	if (len == 31) {
		is_devided = true;
	} else {
		is_devided = false;
	}
	return m;
}

string md2_tool::gen_str(uint32_t a) {
	string s("");
	uint32_t val;
	for (int i = 0; i < 16; i++) {
		val = ((a & (0x3 << (30 - 2 * i))) >> (30 - 2 * i));
		s += to_string(val);
		s += string(" ");
	}
	return s;
}

void md2_tool::md2() {
	uint32_t* p_16bytes = new uint32_t[blocks + 2];

	// step1, step2
	bool is_devided = true;
	uint32_t checksum = 0x00, L = 0x00;
	for (int i = 0; i < blocks - 1; i++) {
		p_16bytes[i] = steps_1_2(string(bytes16[i]), checksum, L);
	}
	p_16bytes[blocks - 1] = steps_1_2_last(string(bytes16[blocks - 1]), checksum, L, is_devided);
	if (is_devided) {
		blocks++;
		p_16bytes[blocks - 1] = steps_1_2(string("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"), checksum, L);
	}
	blocks++;
	p_16bytes[blocks - 1] = checksum;

	// step3
	uint32_t buf0, buf1, buf2;
	buf0 = 0x00, buf1 = 0x00, buf2 = 0x00;

	// step4
	uint32_t t;
	for (int i = 0; i < blocks; i++) {
		buf1 = p_16bytes[i];
		buf2 = buf1 ^ buf0;
		
		t = 0x00;
		for (int j = 0; j < 18; j++) {
			for (int k = 0; k < 16; k++) {
				buf0 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf0 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}
			for (int k = 0; k < 16; k++) {
				buf1 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf1 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}
			for (int k = 0; k < 16; k++) {
				buf2 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf2 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}

			t += j;
			t %= 4;
		}
	}

	hash = buf0;
	delete []p_16bytes;
}

void md2_tool::fill_A(int byte, uint32_t *A, string H_i, string H_i1) {
	for (int i = 0; i < 19; i++) {
		A[i] = 0x00;
	}
	A[0] = str_to_int(H_i);
	A[18] = str_to_int(H_i1);
	A[2] = ((uint32_t)byte) << 30;
	
	// fill from bottom to top
	int first_col = 1;
	uint32_t mask, shift;
	for (int line = 17; line >= 3; line--) {
		for (int col = first_col; col <= 15; col++) {
			shift = 30 - 2 * (col - 1);
			mask = 0x3 << shift;
			A[line] ^= (S_proc((A[line + 1] & mask) >> shift)) << (30 - 2 * col);
			
			shift = 30 - 2 * col;
			mask = 0x3 << shift;
			A[line] ^= A[line + 1] & mask;
		}
		first_col++;
	}

	// fill 1 line
	int t = 0x00;
	for (int col = 0; col < 16; col++) {
		shift = 30 - 2 * col;
		mask = 0x3 << shift;
		A[1] ^= (A[0] & mask);
		A[1] ^= (S_proc(t) << shift);
		t = (A[1] & mask) >> shift;
	}

	// fill 2 line
	// t = A[2][0]
	t = (A[2] & (0x3 << 30)) >> 30;
	for (int col = 1; col < 16; col++) {
		shift = (30 - 2 * col);
		mask = 0x3 << shift;
		A[2] ^= (A[1] & mask);
		A[2] ^= (S_proc(t) << shift);
		t = (A[2] & mask) >> shift;
	}

	// fill from top to bottom
	int last_col = 14;
	int last_line = 17;
	uint32_t tmp = 0x00;
	for (int col = last_col; col >= 0; col--) {
		uint32_t shift_base = (30 - 2 * col);
		for (int line = 3; line <= last_line; line++) {
			mask = 0x3 << (shift_base - 2);
			tmp = (A[line - 1] & mask) >> (shift_base - 2);
			tmp ^= (A[line] & mask) >> (shift_base - 2);
			A[line] ^= (S_rev_proc(tmp) << (shift_base));
			shift_base += 2;
		}
		last_line--;
		last_col--;
	}
}

void md2_tool::find_colissions(uint32_t H_i, uint32_t H_i1, key_val *t1, key_val *t2) {
	int ct1 = 0, ct2 = 0;
	bool found = false;
	key_val *p1 = t1;
	key_val *p2 = t2;
	while (!found && ct1 <= 0xffff && ct2 <= 0xffff) {
		if (p1->key < p2->key) {
			p1++;
			ct1++;
		} else if (p1->key > p2->key) {
			p2++;
			ct2++;
		} else {
			int ct = 1;
			while ((p2 + ct)->key == p2->key) {
				ct++;
			}
			while ((p1->key == p2->key) && !found) {
				for (int i = 0; i < ct; i++) {
					uint32_t m = (((uint32_t)(p1->val)) << 16) | ((p2 + i)->val);
					compress(H_i, m);
					
					if (hash == H_i1) {
						found = true;
						cout << gen_str(m) << endl;
						throw std::invalid_argument("find");
						break;
					}
				}
				p1++;
				ct1++;
			}
			p2 += ct;
			ct2 += ct;
		}
	}
}

static inline bool cmp(const key_val &a, const key_val &b) {
	return a.key < b.key;
}

void md2_tool::preimage() {
	uint32_t A[19], B[5], C[5];
	
	key_val *l_table = new key_val[0xffff];
	key_val *r_table = new key_val[0xffff];
	
	// guess A[1][0]
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	try {
		for (int a = 0; a < 4; a++) {
			fill_A(a, A, string(bytes16[0]), string(bytes16[1]));

			// guess B bytes
			for (int b = 0; b <= 0xff; b++) {
				setB(B, b);
				for (uint32_t mleft = 0; mleft <= 0xffff; mleft++) {
					fill_left(mleft, A, B, C, l_table);
				}
				for (uint32_t mright = 0; mright <= 0xffff; mright++) {
					fill_right(mright, A, B, C, r_table);
				}
				std::sort(l_table, l_table + 0xffff + 1, cmp);
				std::sort(r_table, r_table + 0xffff + 1, cmp);
				find_colissions(A[0], A[18], l_table, r_table);
			}
		}
	}
	catch (std::invalid_argument M) {
		;
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = duration_cast<milliseconds>( t2 - t1 ).count() / 1000.0;
	cout << "Time, sec: " << duration << endl;

	delete []r_table;
	delete []l_table;
}