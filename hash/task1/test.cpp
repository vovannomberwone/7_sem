#include <iostream>
#include <string>
#include <algorithm>
#include <stdint.h>
#include <unistd.h>

using namespace std;

struct mstr {
	uint8_t el[16];
	bool operator<(const mstr a) {
		for (int i = 0; i < 8; i++) {
			if (el[i] < a.el[i]) {
				return true;
			} else if (el[i] < a.el[i]) {
				return false;
			}
		}
		return false;
	}
	bool operator>(const mstr a) {
		for (int i = 0; i < 8; i++) {
			if (el[i] > a.el[i]) {
				return true;
			} else if (el[i] < a.el[i]) {
				return false;
			}
		}
		return false;
	}
	bool operator==(const mstr a) {
		for (int i = 0; i < 8; i++) {
			if (el[i] != a.el[i]) {
				return false;
			}
		}
		return true;
	}
};

string gen_str(mstr a) {
	string s("");
	for (int i = 0; i < 16; i++) {
		s += to_string(a.el[i]);
		s += string(" ");
	}
	return s;
}

string gen_str_2(mstr m1, mstr m2) {
	string s("");
	for (int i = 0; i < 8; i++) {
		s += to_string(m1.el[8 + i]);
		s += string(" ");
	}
	for (int i = 0; i < 8; i++) {
		s += to_string(m2.el[8 + i]);
		s += string(" ");
	}
	return s;
}

struct key_val {
	uint8_t key[4], val[4];
};

inline uint32_t S_proc(uint32_t a) {
	uint32_t s[4] = {
		1, 3, 0, 2
	};
	return s[a];
}

inline uint32_t S_rev_proc(uint32_t a) {
	uint32_t s[4] = {
		2, 0, 3, 1
	};
	return s[a];
}

mstr str_to_int(string s) {
	mstr line;

	for (int i = 0; i < 16; i++) {
		line.el[i] = uint8_t(s[2 * i] - '0');
	}
	return line;
}

void fill_A(int byte, mstr *A, string H_i, string H_i1) {
	for (int i = 0; i < 19; i++) {
		for (int j = 0; j < 16; j++) {
			A[i].el[j] = 0;
		}
	}
	A[0] = str_to_int(H_i);
	A[18] = str_to_int(H_i1);
	A[2].el[0] = byte;
	
	// fill from bottom to top
	int first_col = 1;
	uint32_t mask, shift;
	for (int line = 17; line >= 3; line--) {
		for (int col = first_col; col <= 15; col++) {
			A[line].el[col] = (S_proc((A[line + 1].el[col - 1])));
			A[line].el[col] ^= A[line + 1].el[col];
		}
		first_col++;
	}
	
	// fill 1 line
	int t = 0x00;
	for (int col = 0; col < 16; col++) {
		A[1].el[col] ^= (A[0].el[col]);
		A[1].el[col] ^= (S_proc(t));
		t = (A[1].el[col]);
	}

	// fill 2 line
	// t = A[2][0]
	t = (A[2].el[0]);
	for (int col = 1; col < 16; col++) {
		A[2].el[col] ^= (A[1].el[col]);
		A[2].el[col] ^= (S_proc(t));
		t = (A[2].el[col]);
	}

	// fill from top to bottom
	int last_col = 14;
	int last_line = 17;
	uint32_t tmp = 0x00;
	for (int col = last_col; col >= 0; col--) {
		int cur_col = col;
		for (int line = 3; line <= last_line; line++) {
			tmp = 0x00;
			tmp ^= A[line - 1].el[cur_col + 1];
			tmp ^= A[line].el[cur_col + 1];
			A[line].el[cur_col] ^= (S_rev_proc(tmp));
			cur_col--;
		}
		last_line--;
		last_col--;
	}
}

inline void setB(mstr *B, uint32_t bytes) {
	for (int i = 0; i < 4; i++) {
		B[i + 1].el[15] = (bytes >> (2 * i)) & 0x3;
	}
}

inline bool cmp(const mstr &a, const mstr &b) {
	for (int i = 0; i < 8; i++) {
		if (a.el[i] < b.el[i]) {
			return true;
		} else if (a.el[i] > b.el[i]) {
			return false;
		}
	}
	return false;
}

inline void fill_right(uint32_t mright, mstr *A, mstr *B, mstr *C, mstr *r_table) {
	// clear previous computations results
	for (int line = 1; line <= 4; line++) {
		for (int i = 0; i < 15; i++) {
			B[line].el[i] = 0;
		}
	}
	
	// set m0..m7
	uint32_t mask = 0x3 << (2 * 7);
	uint32_t shift = 2 * 7;
	for (int i = 0; i < 8; i++) {
		B[0].el[8 + i] = (mright & mask) >> shift;
		mask >>= 2;
		shift -= 2;
	}

	// compute B
	uint32_t t, tmp;
	for (int col = 14; col >= 7; col--) {
		for (int line = 4; line >= 1; line--) {
			tmp = ((B[line - 1].el[col + 1]));
			tmp ^= ((B[line].el[col + 1]));
			
			B[line].el[col] = (S_rev_proc(tmp));
		}
	}

	// compute C
	for (int i = 0; i < 16; i++) {
		C[0].el[i] = A[0].el[i] ^ B[0].el[i];
	}
	for (int line = 1; line <= 4; line++) {
		tmp = (A[line].el[0]) ^ (A[line + 1].el[0]);
		tmp = S_rev_proc(tmp);
		tmp = (tmp + (4 - ((line & 0x3) - 1))) & 0x3;
		C[line].el[15] = tmp;
	}

	for (int col = 14; col >= 7; col--) {
		for (int line = 1; line <= 4; line++) {
			tmp = (C[line - 1].el[col + 1]);
			tmp ^= (C[line].el[col + 1]);
			
			C[line].el[col] = (S_rev_proc(tmp));
		}
	}

	// store B, C bytes
	for (int i = 0; i <= 8; i++) {
		r_table[mright].el[i] = B[0].el[i];
	}
	for (int i = 1; i <= 4; i++) {
		r_table[mright].el[8 + i - 1] = B[i].el[7];
	}
	for (int i = 1; i <= 4; i++) {
		r_table[mright].el[12 + i - 1] = C[i].el[7];
	}	
}

inline void fill_left(uint32_t mleft, mstr *A, mstr *B, mstr *C, mstr *l_table) {
	// clear previous computations results
	for (int line = 1; line <= 4; line++) {
		for (int i = 0; i < 15; i++) {
			B[line].el[i] = 0;
		}
	}
	
	// set m0..m7
	uint32_t mask = 0x3 << (2 * 7);
	uint32_t shift = 2 * 7;
	for (int i = 0; i < 8; i++) {
		B[0].el[i] = (mleft & mask) >> shift;
		mask >>= 2;
		shift -= 2;
	}

	// compute B
	uint32_t t;
	for (int line = 1; line <= 4; line++) {
		B[line].el[0] ^= (S_proc(A[line].el[15]));
		B[line].el[0] ^= B[line - 1].el[0];
		t = (B[line].el[0]);
		for (int col = 1; col < 8; col++) {
			B[line].el[col] ^= (B[line - 1].el[col]);
			B[line].el[col] ^= (S_proc(t));
			t = (B[line].el[col]);
		}
	}

	// compute C
	for (int i = 0; i < 16; i++) {
		C[0].el[i] = A[0].el[i] ^ B[0].el[i];
	}
	for (int line = 1; line <= 4; line++) {
		C[line].el[0] = (S_proc(B[line].el[15]));
		C[line].el[0] ^= C[line - 1].el[0];
		t = (C[line].el[0]);
		for (int col = 1; col < 8; col++) {
			C[line].el[col] ^= (C[line - 1].el[col]);
			C[line].el[col] ^= (S_proc(t));
			t = (C[line].el[col]);
		}
	}

	// store B, C bytes
	for (int i = 0; i <= 8; i++) {
		l_table[mleft].el[i] = B[0].el[7];
	}
	for (int i = 1; i <= 4; i++) {
		l_table[mleft].el[8 + i - 1] = B[i].el[7];
	}
	for (int i = 1; i <= 4; i++) {
		l_table[mleft].el[12 + i - 1] = C[i].el[7];
	}
}

inline uint32_t compress(mstr &h, mstr &m1, mstr &m2) {
	uint32_t buf0, buf1, buf2;
	buf0 = 0x00, buf1 = 0x00, buf2 = 0x00;

	uint32_t h_i = 0;
	uint32_t m_i = 0;
	for (int i = 0; i < 8; i++) {
		h_i <<= 2;
		m_i <<= 2;
		h_i |= h.el[i];
		m_i |= m1.el[8 + i];
	}
	for (int i = 0; i < 8; i++) {
		h_i <<= 2;
		m_i <<= 2;
		h_i |= h.el[8 + i];
		m_i |= m2.el[8 + i];
	}

	// step4
	uint32_t t, trash;
	buf0 = h_i;
	buf1 = m_i;
	buf2 = buf1 ^ buf0;
	
	t = 0x00;
	for (int j = 0; j < 18; j++) {
		for (int k = 0; k < 16; k++) {
			buf0 ^= (S_proc(t) << (30 - 2 * k));
			t = (buf0 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
		}
		for (int k = 0; k < 16; k++) {
			buf1 ^= (S_proc(t) << (30 - 2 * k));
			t = (buf1 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
		}
		for (int k = 0; k < 16; k++) {
			buf2 ^= (S_proc(t) << (30 - 2 * k));
			t = (buf2 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
		}

		t += j;
		t %= 4;
	}
	
	return buf0;
}

uint32_t gen_int(mstr h) {
	uint32_t h_i = 0;
	for (int i = 0; i < 16; i++) {
		h_i <<= 2;
		h_i |= h.el[i];
	}
	return h_i;
}

void find_colissions(mstr H_i, mstr H_i1, mstr *t1, mstr *t2) {
	int ct1 = 0, ct2 = 0;
	bool found = false;
	mstr *p1 = t1;
	mstr *p2 = t2;
	while (!found && ct1 <= 0xffff && ct2 <= 0xffff) {
		if (*p1 < *p2) {
			p1++;
			ct1++;
		} else if (*p1 > *p2) {
			p2++;
			ct2++;
		} else {
			cout << "HERE" << endl;
			int ct = 1;
			while (*(p2 + ct) == *p2) {
				ct++;
			}
			while ((*p1 == *p2) && !found) {
				for (int i = 0; i < ct; i++) {
					uint32_t hash = compress(H_i, *p1, *p2);
					if (hash == gen_int(H_i1)) {
						//found = true;
						//cout << gen_str_2(*p1, *p2) << endl;
						if ("0 0 1 2 2 3 2 2 0 1 3 1 0 3 3 1" == gen_str_2(*p1, *p2)) {
							cout << "sdadsadsadas" << endl;
						}
						//break;
					}
				}
				p1++;
				ct1++;
			}
			p2 += ct;
			ct2 += ct;
		}
	}
}

void preimage() {
	mstr A[19], B[5], C[5];
	
	mstr *l_table = new mstr[0xffff];
	mstr *r_table = new mstr[0xffff];
	
	// guess A[1][0]
	for (int a = 0; a < 4; a++) {
		//fill_A(a, A, string("0 2 2 3 2 3 0 2 1 1 0 0 2 2 2 0"), string("3 0 0 2 3 0 2 3 2 3 3 0 1 0 3 2"));
		//fill_A(a, A, string("0 1 2 3 0 1 2 3 0 1 2 3 0 1 2 3"), string("0 0 3 2 2 2 1 0 0 0 3 0 3 3 1 2"));
		fill_A(a, A, string("3 2 0 3 2 0 1 0 3 2 2 3 3 0 3 0"), string("2 1 3 3 0 2 0 3 2 0 1 0 2 2 1 0"));
		
		// guess B bytes
		for (int b = 0; b <= 0xff; b++) {
			setB(B, b);
			for (uint32_t mleft = 0; mleft <= 0xffff; mleft++) {
				fill_left(mleft, A, B, C, l_table);
			}
			for (uint32_t mright = 0; mright <= 0xffff; mright++) {
				fill_right(mright, A, B, C, r_table);
				/*
				for (int i = 0; i < 19; i++) {
					cout << gen_str(A[i]) << endl;
				}
				cout << "-----" << endl;
				for (int i = 0; i < 5; i++) {
					cout << gen_str(B[i]) << endl;
				}
				cout << "-----" << endl;
				for (int i = 0; i < 5; i++) {
					cout << gen_str(C[i]) << endl;
				}
				pause();
				*/
			}
		}
		std::sort(l_table, l_table + 0xffff + 1, cmp);
		std::sort(r_table, r_table + 0xffff + 1, cmp);
		find_colissions(A[0], A[18], l_table, r_table);
	}
	delete []r_table;
	delete []l_table;
}

int main() {
	preimage();
}