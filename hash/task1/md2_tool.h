#pragma once

#include "parser.h"
#include <stdint.h>
#include <string>

struct key_val {
	uint16_t key, val;
};

class md2_tool {
private:
	uint32_t hash;
	char const **bytes16;
	int blocks;

	void md2();
	
	inline uint32_t S_proc(uint32_t a) {
		uint32_t s[4] = {
			1, 3, 0, 2
		};
		return s[a];
	}

	inline uint32_t S_rev_proc(uint32_t a) {
		uint32_t s[4] = {
			2, 0, 3, 1
		};
		return s[a];
	}
	
	inline void fill_left(uint32_t mleft, uint32_t *A, uint32_t *B, uint32_t *C, key_val *l_table) {
		// clear previous computations results
		for (int line = 1; line <= 4; line++) {
			B[line] &= 0x3;
		}
		
		// set m0..m7
		B[0] = mleft << 16;

		// compute B
		uint32_t t, shift, mask;
		for (int line = 1; line <= 4; line++) {
			B[line] ^= (S_proc(A[line] & 0x3)) << 30;
			B[line] ^= B[line - 1] & (0x3 << 30);
			t = (B[line] & (0x3 << 30)) >> 30;
			for (int col = 1; col < 8; col++) {
				shift = (30 - 2 * col);
				mask = 0x3 << shift;
				B[line] ^= (B[line - 1] & mask);
				B[line] ^= (S_proc(t) << shift);
				t = (B[line] & mask) >> shift;
			}
		}

		// compute C
		C[0] = A[0] ^ B[0];
		for (int line = 1; line <= 4; line++) {
			C[line] = (S_proc(B[line] & 0x3)) << 30;
			C[line] ^= C[line - 1] & (0x3 << 30);
			t = (C[line] & (0x3 << 30)) >> 30;
			for (int col = 1; col < 8; col++) {
				shift = (30 - 2 * col);
				mask = 0x3 << shift;
				C[line] ^= (C[line - 1] & mask);
				C[line] ^= (S_proc(t) << shift);
				t = (C[line] & mask) >> shift;
			}
		}

		// store B, C bytes
		uint16_t res = 0x00;
		shift = 30 - 2 * 7;
		mask = 0x3 << shift;
		for (int line = 1; line <= 4; line++) {
			res <<= 2;
			res |= ((B[line] & mask) >> shift); 
		}
		for (int line = 1; line <= 4; line++) {
			res <<= 2;
			res |= ((C[line] & mask) >> shift); 
		}
		l_table[mleft].key = res;
		l_table[mleft].val = mleft;
	}

	inline void fill_right(uint32_t mright, uint32_t *A, uint32_t *B, uint32_t *C, key_val *r_table) {
		// clear previous computations results
		for (int line = 1; line <= 4; line++) {
			B[line] &= 0x3;
		}
		
		// set m0..m7
		B[0] = mright;

		// compute B
		uint32_t t, shift, mask, tmp;
		for (int col = 14; col >= 7; col--) {
			for (int line = 4; line >= 1; line--) {
				shift = (30 - 2 * (col - 1));
				mask = 0x3 << shift;
				tmp = ((B[line - 1] & mask) >> shift);
				tmp ^= ((B[line] & mask) >> shift);
				
				shift += 2;
				B[line] ^= (S_rev_proc(tmp) << shift);
			}
		}

		// compute C
		C[0] = A[0] ^ B[0];
		shift = 30;
		mask = 0x3 << shift;
		for (int line = 1; line <= 4; line++) {
			tmp = ((A[line] & mask) >> shift) ^ ((A[line + 1] & mask) >> shift);
			tmp = S_rev_proc(tmp);
			tmp = (tmp + (4 - ((line & 0x3) - 1))) & 0x3;
			C[line] = tmp;
		}

		for (int col = 14; col >= 7; col--) {
			for (int line = 1; line <= 4; line++) {
				shift = (30 - 2 * (col + 1));
				mask = 0x3 << shift;
				tmp = (C[line - 1] & mask) >> shift;
				tmp ^= (C[line] & mask) >> shift;
				
				shift += 2;
				C[line] ^= (S_rev_proc(tmp) << shift);
			}
		}

		// store B, C bytes
		uint16_t res = 0x00;
		shift = 30 - 2 * 7;
		mask = 0x3 << shift;
		for (int line = 1; line <= 4; line++) {
			res <<= 2;
			res |= ((B[line] & mask) >> shift); 
		}
		for (int line = 1; line <= 4; line++) {
			res <<= 2;
			res |= ((C[line] & mask) >> shift); 
		}
		r_table[mright].key = res;
		r_table[mright].val = mright;	
	}

	inline void setB(uint32_t *B, uint32_t bytes) {
		for (int i = 0; i < 4; i++) {
			B[i + 1] = (bytes >> (2 * i)) & 0x3;
		}
	}

	static inline bool cmp(const key_val &a, const key_val &b) {
		return a.key < b.key;
	}

	inline void compress(uint32_t h_i, uint32_t m_i) {
		uint32_t buf0, buf1, buf2;
		buf0 = 0x00, buf1 = 0x00, buf2 = 0x00;

		// step4
		uint32_t t, trash;
		buf0 = h_i;
		buf1 = m_i;
		buf2 = buf1 ^ buf0;
		
		t = 0x00;
		for (int j = 0; j < 18; j++) {
			for (int k = 0; k < 16; k++) {
				buf0 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf0 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}
			for (int k = 0; k < 16; k++) {
				buf1 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf1 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}
			for (int k = 0; k < 16; k++) {
				buf2 ^= (S_proc(t) << (30 - 2 * k));
				t = (buf2 & (0x3 << (30 - 2 * k))) >> (30 - 2 * k);
			}

			t += j;
			t %= 4;
		}
		
		hash = buf0;
	}
	void preimage();

	uint32_t str_to_int(string s);
	string gen_str(uint32_t a);
	uint32_t steps_1_2(const std::string s, uint32_t &checksum, uint32_t &L);
	uint32_t steps_1_2_last(const std::string s, uint32_t &checksum, uint32_t &L, bool &is_devided);
	void fill_A(int byte, uint32_t *A, string H_i, string H_i1);
	void find_colissions(uint32_t H_i, uint32_t H_i1, key_val *t1, key_val *t2);
public:
	md2_tool() = default;
	~md2_tool() = default;
	void run(const parser::mode m, char const **bytes16, const int ct);
};