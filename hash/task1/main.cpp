#include <iostream>
#include "md2_tool.h"
#include "parser.h"

int main(int argc, char const **argv) {
	parser p;
	parser::mode m = p.parse_request(argv);
	md2_tool tool;
	tool.run(m, argv + 2, argc - 2);
	return 0;
}