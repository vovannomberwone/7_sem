#include "gost_attack.h"
#include <iostream>
#include <bitset>
#include <unistd.h>
#include <unordered_map>
#include <chrono>

using std::cout;
using std::unordered_map;
using std::string;
using std::hex;
using std::endl;

using namespace std::chrono;

U64 gost_attack::str_to_int(const string s) {
	U64 m = 0x00;
	U64 cur_byte;

	for (int i = 0; i < 32; i++) {
		m <<= 2;
		cur_byte = uint32_t(s[2 * i] - '0');
		m |= cur_byte;
	}
	return m;
}

string gost_attack::int_to_str(U64 a) {
	string s("");
	U64 val;
	for (int i = 0; i < 32; i++) {
		val = ((a & (static_cast<uint64_t>(0x3) << (62 - 2 * i))) >> (62 - 2 * i));
		s += std::to_string(val);
		s += string(" ");
	}
	return s;
}

void gost_attack::test_S() {
	cout << "test_S" << endl;
	U64 M = 0x2a;
	U64 r1 = S(M);

	cout << "M   : " << hex << M << endl;
	cout << "S   : " << hex << r1 << endl;
	cout << "S_rev: " << hex << S_rev(r1) << endl;
}

void gost_attack::test_rotate() {
	cout << "test_rotate" << endl;
	U64 M = 0xa2;
	U64 r1 = rol3(M);

	cout << "M   : " << std::bitset<16>(M) << endl;
	cout << "Rol : " << std::bitset<16>(r1) << endl;
	cout << "Ror : " << hex << std::bitset<16>(ror3(r1)) << endl;
}

void gost_attack::test_fixed_point() {
	cout << "test_fixed_point" << endl;
	U64 M = 0x1a1a;
	U64 K = construct_fixed_point(M);

	cout << "M   : " << hex << M << endl;
	cout << "K   : " << hex << K << endl;
	cout << "E(M): " << hex << E(M, K) << endl;
}

void gost_attack::test_E() {
	cout << "test_E" << endl;
	U64 M = 0x1a1a;
	U64 K = 0x2222333344445555;

	cout << "M   : " << hex << M << endl;
	cout << "K   : " << hex << K << endl;
	cout << "E(M): " << hex << E(M, K) << endl;
}

void gost_attack::test_P() {
	cout << "test_P" << endl;
	U64 M = 0x1111222233334444;
	U64 R = P(M);
	
	cout << int_to_str(M) << endl;
	cout << int_to_str(R) << endl;

	cout << "M       : " << hex << M << endl;
	cout << "P(M)    : " << hex << R << endl;
	cout << "P_rev(M): " << hex << P_rev(R) << endl;
}

void gost_attack::test_f() {
	cout << "test_fixed_point" << endl;
	U64 M = 0x1212121234343434;
	U64 H = 0x1a2b3c4d5e6f7788;

	cout << "M   : " << hex << M << endl;
	cout << "H   : " << hex << H << endl;
	cout << "f(M, H): " << hex << f(M, H) << endl;
}

void gost_attack::find_collisions(const string s) {
	const unsigned long COUNT = 16777216; // 2^24
	const U64 z0 = 0x00;
	
	U64 H = str_to_int(s);
	unordered_map<U64, U64> table(COUNT);

	if ((H & 0xff) != ((H & 0xff00) >> 8)) {
		cout << "h0 is not symmetric" << endl;
		return;
	}

	U64 h0 = H & 0xffff;
	U64 k0, m, tmp;
	//high_resolution_clock::time_point t1 = high_resolution_clock::now();
	while (1) {	
		k0 = construct_fixed_point(h0);
		m = P_rev(k0) ^ H;
		tmp = m;
		for (int j = 0; j < 12; j++) {
			tmp = psi_rev(tmp);
		}
		if ((tmp & 0xffff) == z0) {
			auto iter = table.find(f(m, H));
			if (iter != table.end() && m != iter->second) {
				cout << "FIND " << m << " " << (iter->second) << endl;
				break;
			} else {
				table.insert(std::pair<U64, U64>(f(m, H), m));
			}
		}
		/*
		if (table.size() % 0x10000 == 0) {
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			auto duration = duration_cast<seconds>( t2 - t1 ).count();
			cout << "Time, sec: " << dec << duration << " size " << table.size() << endl;
		}
		*/
	}
}