#pragma once

#include <stdint.h>
#include <random>

#define U16 uint16_t
#define U32 uint32_t
#define U64 uint64_t

using std::string;

class gost_attack {
private:
	std::mt19937_64 gen;
	U64 cur_k;

	// real a size is 2 bit
	inline U64 Sbox(U64 a) {
		const static U64 s[4] = {
			1, 3, 0, 2
		};
		return s[a];
	}

	inline U64 Sbox_rev(U64 a) {
		const static U64 s[4] = {
			2, 0, 3, 1
		};
		return s[a];
	}
	
	// real tmp size is 8 bit
	inline U64 S(U64 tmp) {
		U64 mask, shift, cur;
		U64 res = 0x00;

		for (int i = 3; i >= 0; i--) {
			shift = 2 * i;
			mask = static_cast<uint64_t>(0x3) << shift;
			cur = Sbox((tmp & mask) >> shift);
			res |= (cur << shift);
		}
		return res;
	}

	inline U64 S_rev(U64 tmp) {
		U64 mask, shift, cur;
		U64 res = 0x00;

		for (int i = 3; i >= 0; i--) {
			shift = 2 * i;
			mask = static_cast<uint64_t>(0x3) << shift;
			cur = Sbox_rev((tmp & mask) >> shift);
			res |= (cur << shift);
		}
		return res;
	}

	// real a size is 8 bit
	inline U64 rol3(U64 a) {
		return (a >> 5) | ((a & 0x1f) << 3);
	}

	inline U64 ror3(U64 a) {
		return (a >> 3) | ((a & 0x7) << 5);
	}

	// real M size is 16 bit
	inline U64 E(U64 M, U64 K) {
		U64 L = (M >> 8) & 0xff;
		U64 R = M & 0xff;
		U64 tmp;
		U64 key;
		U64 mask, shift;

		for (int i = 0; i < 32; i++) {
			tmp = R;
			if (i < 24) {
				shift = 8 * (i % 8);
			} else {
				shift = 8 * (7 - i % 8);
			}
			mask = static_cast<uint64_t>(0xff) << shift;
			key = (K & mask) >> shift;

			tmp += key;
			tmp &= 0xff;
			tmp = S(tmp);
			tmp = rol3(tmp);
			tmp ^= L;
			L = R;
			R = tmp;
		}

		// in last iter of cicle L and R arent swapped
		return (R << 8) | L;
	}

	// assume L == R
	inline U64 construct_fixed_point(U64 M) {
		U64 K = gen();
		K >>= 16;
	
		//U64 K = cur_k++;
		//K &= static_cast<uint64_t>(0xffff) << 48;

		U64 L = (M >> 8) & 0xff;
		U64 R = M & 0xff;
		U64 tmp;
		U64 key;
		U64 mask, shift;

		for (int i = 0; i < 6; i++) {
			tmp = R;
			shift = 8 * (i % 8);
			mask = static_cast<uint64_t>(0xff) << shift;
			key = (K & mask) >> shift;

			tmp += key;
			tmp &= 0xff;
			tmp = S(tmp);
			tmp = rol3(tmp);
			tmp ^= L;
			L = R;
			R = tmp;
		}

		U64 k7, k8;
		U64 CL = (M >> 8) & 0xff;
		U64 CR = M & 0xff;

		k7 = S_rev(ror3(CL ^ L)) - R;
		k7 &= 0xff;
		k8 = S_rev(ror3(CR ^ R)) - CL;
		k8 &= 0xff;
		K |= (k7 << (6 * 8));
		K |= (k8 << (7 * 8));

		return K;
	}

	inline U64 construct_c2(const char * s) {
		U64 res = 0x00;
		/*
		for (int i = 0; i < 32; i += 2) {
			res <<= 2;
			if (s[i] - '0') {
				res |= 0x3;
			}
		}
		*/
		for (int i = 0; i < 16; i += 1) {
			res <<= 4;
			if (s[i] - '0') {
				res |= 0xf;
			}
		}
		return res;
	}

	inline U64 A(U64 y) {
		U64 y1 = y & 0xffff;
		U64 y2 = (y & 0xffff0000) >> 16;
		return (y >> 16) | ((y1 ^ y2) << 48);
	}

	inline U64 P(U64 y) {
		U64 tmp = y;
		U64 mask, shift; 
		y = 0x00;
		for (int k = 7; k >= 0; k--) {
			for (int i = 3; i >= 0; i--) {
				y <<= 2;
				shift = 2 * (8 * i + k);
				mask = static_cast<uint64_t>(0x3) << shift;
				y |= ((tmp & mask) >> shift);
			}
		}
		return y;
	}

	inline U64 P_rev(U64 y) {
		U64 tmp = y;
		U64 mask, shift; 
		y = 0x00;
		for (int i = 3; i >= 0; i--) {
			for (int k = 7; k >= 0; k--) {
				y <<= 2;
				shift = 2 * (i + 4 * k);
				mask = static_cast<uint64_t>(0x3) << shift;
				y |= ((tmp & mask) >> shift);
			}
		}
		return y;
	}

	inline U64 psi(U64 y) {
		static const int pos[6] = {1, 2, 3, 4, 13, 16};
		U64 res = 0x00, tmp = 0x00;
		U64 mask, shift;
		for (int i = 0; i < 6; i++) {
			shift = 4 * (pos[i] - 1);
			mask = static_cast<uint64_t>(0x0f) << shift;
			tmp ^= (y & mask) >> shift;
		}
		res = (y >> 4) | (tmp << 60);
		return res;
	}

	inline U64 psi_rev(U64 y) {
		static const int pos[5] = {2, 3, 4, 13, 16};
		U64 res = 0x00, sum;
		U64 mask, shift;
		sum = ((y & (static_cast<U64>(0x3) << 60)) > 60);
		y <<= 4;
		for (int i = 0; i < 5; i++) {
			shift = 4 * (pos[i] - 1);
			mask = static_cast<uint64_t>(0x0f) << shift;
			sum ^= (y & mask) >> shift;
		}
		res = y | sum;
		return res;
	}

	inline U64 f(U64 M, U64 H) {
		// key_gen
		U64 K[4];
		const U64 C[3] {
			0x00,
			construct_c2("ff00ffff000000ffff0000ff00ffff0000ff00ff00ff00ffff00ff00ff00ff00"),
			0x00
		};
		U64 U = H;
		U64 V = M;
		U64 W = U ^ V;
		K[0] = P(W);

		for (int i = 0; i < 3; i++) {
			U = A(U) ^ C[i];
			V = A(A(V));
			W = U ^ V;
			K[1 + i] = P(W);
		}

		// encrypt
		U64 S = 0x00;
		U64 tmp = M;
		U64 shift, mask;
		for (int i = 3; i >= 0; i--) {
			S <<= 16;
			shift = 16 * i;
			mask = static_cast<uint64_t>(0xffff) << shift;
			tmp = (H & mask) >> shift;
			S |= E(tmp, K[i]);
		}

		// mix
		U64 res = psi(S);
		for (int i = 0; i < 11; i++) {
			res = psi(res);
		}
		res ^= M;
		res = psi(res);
		res ^= H;
		for (int i = 0; i < 61; i++) {
			res = psi(res);
		}
		return res;
	}

	U64 str_to_int(const string s);
	string int_to_str(U64 a);
public:
	gost_attack() {
		gen.seed(1);
		cur_k = 0x00;
	}
	~gost_attack() = default;
	void find_collisions(const string s);
	void test_fixed_point();
	void test_S();
	void test_E();
	void test_P();
	void test_f();
	void test_rotate();
};