#include "gost_attack.h"
#include <unistd.h> 

int main(int argc, char const **argv) {
	gost_attack tool;
	tool.test_S();
	tool.test_rotate();
	tool.test_fixed_point();
	tool.test_E();
	tool.test_P();
	tool.test_f();
	tool.find_collisions(argv[1]);
	return 0;
}