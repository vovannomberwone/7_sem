#include "chacha.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

using std::fstream;
using std::ios;
using std::cout;
using std::endl;
using std::hex;
using std::setfill;
using std::setw;

void chacha::test_QUARTERROUND_BASE() {
	cout << "Test QUARTERROUND_BASE" << endl;
	U32 a = 0x11111111;
	U32 b = 0x01020304;
	U32 c = 0x9b8d6f43;
	U32 d = 0x01234567;
	QUARTERROUND_BASE(a, b, c, d);
	cout << "a = " << setw(8) << setfill('0') << hex << a << endl;
	cout << "b = " << setw(8) << setfill('0') << hex << b << endl;
	cout << "c = " << setw(8) << setfill('0') << hex << c << endl;
	cout << "d = " << setw(8) << setfill('0') << hex << d << endl;
	cout << endl;
}

void chacha::test_QUARTERROUND() {
	cout << "Test QUARTERROUND" << endl;
	U32 state[16] = {
		0x879531e0, 0xc5ecf37d, 0x516461b1, 0xc9a62f8a,
		0x44c20ef3, 0x3390af7f, 0xd9fc690b, 0x2a5f714c,
		0x53372767, 0xb00a5631, 0x974c541a, 0x359e9963,
		0x5c971061, 0x3d631689, 0x2098d9d6, 0x91dbd320
	};
	for (int i = 0; i < 16; i++) {
		this->state[i] = state[i];
	}
	QUARTERROUND(2, 7, 8, 13);
	for (int i = 0; i < 16; i++) {
		cout << setw(8) << setfill('0') << hex << this->state[i];
		if ((i + 1) % 4 == 0) {
			cout << endl;
		} else {
			cout << " ";
		}
	}
	cout << endl;
}

void chacha::test_ChaCha20_block() {
	cout << "Test ChaCha20_block" << endl << "See ../tests/block_out.bin";
	U32 key[8], nonce[3], out[16], counter[1];
	counter[0] = 0x01;

	fstream f_key("../tests/block_key.bin", ios::binary | ios::in);
	fstream f_nonce("../tests/block_nonce.bin", ios::binary | ios::in);
	fstream f_out("../tests/block_out.bin", ios::binary | ios::out | ios::trunc);

	f_key.read((char*)key, 32);
	f_nonce.read((char*)nonce, 12);

	ChaCha20_block(key, nonce, counter, out);
	for (int i = 0; i < 16; i++) {
		cout << setw(8) << setfill('0') << hex << out[i] << endl;
	}
	f_out.write((char *)out, 64);

	f_key.close();
	f_nonce.close();
	f_out.close();
	cout << endl;
}

void chacha::ChaCha20(const U32* const key, const U32* const nonce, U32* const counter, const U32* const in, U32* const out, int len) {
	U32 tmp_out[16];
	for (int i = 0; i < (len + 63) / 64; i++) {
		ChaCha20_block(key, nonce, counter, tmp_out);
		(*counter)++;
		for (int j = 0; j < 16; j++) {
			out[i * 16 + j] = in[i * 16 + j] ^ tmp_out[j];
		}
	}
}

void chacha::test_ChaCha20() {
	cout << "Test ChaCha20" << endl << "See ../tests/out.bin";
	U32 key[8], nonce[3], counter[1];
	counter[0] = 0x01;

	fstream f_in("../tests/in.bin", ios::binary | ios::in);
	fstream f_key("../tests/key.bin", ios::binary | ios::in);
	fstream f_nonce("../tests/nonce.bin", ios::binary | ios::in);
	fstream f_out("../tests/out.bin", ios::binary | ios::out | ios::trunc);

	f_key.read((char*)key, 32);
	f_nonce.read((char*)nonce, 12);
	unsigned long len = fs::file_size("../tests/in.bin");
	U32* in = new U32[(len + 63) / 64 * (64 / sizeof(U32))];
	for (int i = 0; i < (len / 64); i++) {
		f_in.read((char*)in + i * 64, 64);
	}
	if (len % 64) {
		f_in.read((char*)in + len / 64 * 64, len % 64);
	}

	ChaCha20(key, nonce, counter, in, in, len);
	
	for (int i = 0; i < len / 64; i++) {
		f_out.write((char*)in, 64);
	}
	if (len % 64) {
		f_out.write((char*)in + len / 64 * 64, len % 64);
	}

	f_in.close();
	f_key.close();
	f_nonce.close();
	f_out.close();
	delete []in;
	cout << endl;
}