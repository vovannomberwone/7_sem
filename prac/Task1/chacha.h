#include <stdint.h>

#define U32 uint32_t
#define U64 uint64_t

class chacha {
private:
	U32 state[16];
	
	void __attribute__((always_inline)) QUARTERROUND_BASE(U32 &a, U32 &b, U32 &c, U32 &d) {
		a += b; d ^= a; d = ((d & 0xffff0000) >> 16) | (d << 16);
		c += d; b ^= c; b = ((b & 0xfff00000) >> 20) | (b << 12);
		a += b; d ^= a; d = ((d & 0xff000000) >> 24) | (d << 8);
		c += d; b ^= c; b = ((b & 0xfe000000) >> 25) | (b << 7);
	}
	
	void __attribute__((always_inline)) QUARTERROUND(U32 a, U32 b, U32 c, U32 d) {
		QUARTERROUND_BASE(state[a], state[b], state[c], state[d]);
	}

	void __attribute__((always_inline)) set_state(const U32* const key, const U32* const nonce, const U32* const counter) {
		state[0] = 0x61707865;
		state[1] = 0x3320646e;
		state[2] = 0x79622d32;
		state[3] = 0x6b206574;
		for (int i = 0; i < 8; i++) {
			state[4 + i] = key[i];
		}
		state[12] = *counter;
		for (int i = 0; i < 3; i++) {
			state[13 + i] = nonce[i];
		}
	}

	void __attribute__((always_inline)) inner_block() {
		QUARTERROUND(0, 4, 8, 12);
   		QUARTERROUND(1, 5, 9, 13);
   		QUARTERROUND(2, 6, 10, 14);
   		QUARTERROUND(3, 7, 11, 15);
   		QUARTERROUND(0, 5, 10, 15);
   		QUARTERROUND(1, 6, 11, 12);
   		QUARTERROUND(2, 7, 8, 13);
   		QUARTERROUND(3, 4, 9, 14);	
	}

	void __attribute__((always_inline)) ChaCha20_block(const U32* const key, const U32* const nonce, const U32* const counter, U32* const out) {
		set_state(key, nonce, counter);
		U32 init_state[16];
		for (int i = 0; i < 16; i++) {
			init_state[i] = state[i];
		}
		for (int i = 0; i < 10; i++) {
			inner_block();
		}
		for (int i = 0; i < 16; i++) {
			state[i] += init_state[i];
			out[i] = state[i];
		}
	}
public:
	chacha() = default;
	~chacha() = default;
	void ChaCha20(const U32* const key, const U32* const nonce, U32* const counter, const U32* const in, U32* const out, int len);
	
	void test_QUARTERROUND_BASE();
	void test_QUARTERROUND();
	void test_ChaCha20_block();
	void test_ChaCha20();
};