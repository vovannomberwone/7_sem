#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

using namespace std;

int pos(char *str, char c) {
    int pos = 0;
    while (*str != '\0') {
        if (*str == c) {
            return pos;
        }
        pos++;
        str++;
    }
    return -1;
}

int main(int argc, char **argv) {
    int a, b;
    char alph[] = "0123456789abcdef";
    cout << "In: " << argv[1] << " Out: " << argv[2] << endl; 
    
    FILE *txt_file = fopen(argv[1], "rt");
    int bin_file = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    
    a = fgetc(txt_file);
    while (a != EOF) {
        b = fgetc(txt_file);
        if (b == EOF) {
            printf("ERROR NOT COMPLETE BYTE\n");
            printf("Last symbol is: %c\n", b);
            break;
        }
        unsigned char c = pos(alph, a);
        c <<= 4;
        c |= pos(alph, b);
        write(bin_file, &c, 1);
        a = fgetc(txt_file);
        if (a == EOF) {
            break;
        } else {
            a = fgetc(txt_file);
        }
    }

    fclose(txt_file);
    close(bin_file);
    return 0;
}